# Remember, Remember

The 5th of November.
This Delta Chat bot
sends out reminders once a day,
depending on an input file.

## Setup

```
python3 -m venv venv
. venv/bin/activate
echo "REMEMBER_EMAIL=diary@testrun.org" > .env
echo "REMEMBER_PASSWORD=`pass show other/diary@testrun.org`" >> .env
pip install -e .
pyinfra @local local-deploy.py
```
