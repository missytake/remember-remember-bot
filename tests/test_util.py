from remember_remember_bot.util import (
    check_new_day,
    update_day,
    chat_is_active,
    get_file_path,
    today_lines,
)
import pytest
import datetime


def test_update_day():
    current_day = 0
    assert check_new_day(current_day)
    current_day = update_day()
    assert not check_new_day(current_day)


@pytest.mark.parametrize(
    ("messages", "active"),
    [
        (["/start"], True),
        (["/start", "/stop"], False),
        (["/start", "/stop", "Let's get this party started"], False),
        (
            [
                "/start",
                "Message deletion timer is set to 1 day by",
                "Message deletion timer is disabled by",
                "I will stop sending you messages now.",
            ],
            False,
        ),
        (["/start", "/stop", "/start"], True),
    ],
)
def test_chat_is_active(messages, active, chat):
    for msg in messages:
        chat.send_text(msg)
    assert chat_is_active(chat) == active


def test_today_lines():
    day = datetime.datetime.now().day
    month = datetime.datetime.now().month
    year = datetime.datetime.now().year
    lines = [
        f"{day}.{month}.{year}\tToday I had a great day!",
        f"{day}.{month}.2020\tI didn't go outside all day :) no appointments",
        "03.13.2024\tNice how they invented a 13th month extra for the Satanists!",
    ]
    result = today_lines(lines)
    print(result)
    assert len(result) == 2
    assert result[0] == lines[0]
    assert result[1] == lines[1]
