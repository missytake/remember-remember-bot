import time

import deltachat

from remember_remember_bot.commands import store_file, reply
from remember_remember_bot.util import get_file_path, get_lines_from_file


def test_store_file(tmp_file_path, chat: deltachat.Chat):
    chat.set_ephemeral_timer(1)
    reply(chat, "/file", attachment=str(tmp_file_path))
    last_message = chat.get_messages()[-1]
    store_file(last_message)
    time.sleep(1)
    new_file_path = get_file_path(chat)
    assert new_file_path
    assert get_lines_from_file(new_file_path) == ["test"]
    assert chat.get_ephemeral_timer() == 1
