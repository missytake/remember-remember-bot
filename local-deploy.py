import os
import importlib.resources

from pyinfra.operations import apt, files, pip, server, systemd


# install into .local/lib
def install(unix_user, enable_linger=True) -> None:
    remember_package = importlib.resources.files("remember_remember_bot")
    working_directory = os.getcwd()
    remote_venv_dir = f"{working_directory}/venv"

    # install systemd units
    fn = "remember-remember-bot"
    service_file = remember_package / "pyinfra_assets" / f"{fn}.service.j2"
    if enable_linger:
        server.shell(
            name=f"enable {unix_user}'s systemd units to auto-start at boot",
            commands=[f"loginctl enable-linger {unix_user}"],
        )
        files.line(
            name=f"{unix_user} .profile: ensure XDG_RUNTIME_DIR",
            path=f"/home/{unix_user}/.profile",
            line='export XDG_RUNTIME_DIR="/run/user/$UID"',
        )
        files.line(
            name=f"{unix_user} .profile: ensure DBUS_SESSION_BUS_ADDRESS",
            path=f"/home/{unix_user}/.profile",
            line='export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"',
        )

    files.template(
        name=f"Upload {fn}.service",
        src=service_file,
        dest=f"/home/{unix_user}/.config/systemd/user/{fn}.service",
        execpath=f"{remote_venv_dir}/bin/{fn}",
        working_directory=working_directory,
        unix_user=unix_user,
    )
    systemd.service(
        name=f"Setup {fn} service",
        service=f"{fn}.service",
        running=True,
        enabled=True,
        restarted=True,
        daemon_reload=True,
        user_mode=True,
    )


unix_user = os.environ.get("REMEMBER_PYINFRA_USER", os.environ.get("USER"))
install(unix_user)
