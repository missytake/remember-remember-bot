import datetime

import deltachat


def check_new_day(current_day):
    if current_day != datetime.datetime.now().day:
        return True
    return False


def update_day():
    return datetime.datetime.now().day


def chat_is_active(chat: deltachat.Chat) -> bool:
    """Check if a user activated the bot.

    :param messages: the text of all messages with the user
    :return: True if the user wants to receive reminders.
    """
    messages = [msg.text for msg in chat.get_messages()]
    active = False
    for message_text in messages:
        if "/start" in message_text:
            active = True
        if "/stop" in message_text:
            active = False
        if "I will stop sending you messages now." in message_text:
            active = False
        if (
            "You first need to send me a file path with the /file command."
            in message_text
        ):
            active = False
    return active


def get_file_path(chat: deltachat.Chat) -> str:
    """Get the file path from the user's chat.

    :param chat: the chat in which the file is requested
    :return: the file path with their reminders
    """
    return "./files/" + str(chat.id)


def get_lines_from_file(file_path: str) -> [str]:
    """Get the lines from the file with the user's reminders.

    :param file_path: the path to the file with the user's reminders
    :return: a list with all the user's reminders
    """
    try:
        with open(file_path, "r", encoding="utf-8") as f:
            lines = f.readlines()
    except (FileNotFoundError, TypeError):
        return [f"File not found: {file_path}"]
    return [line.strip("\n") for line in lines]


def today_lines(lines: [str]):
    """Out of several lines from the file, only return those with the same date as today.

    :param lines: a list of lines with a date in the beginning.
    :return: a list of lines, all from today.
    """
    result = []
    for line in lines:
        try:
            day = int(line.split(".")[0])
            month = int(line.split(".")[1])
        except ValueError:
            print(f"wrong format: {line}")
            continue
        if (
            day == datetime.datetime.now().day
            and month == datetime.datetime.now().month
        ):
            result.append(line)
    return result
