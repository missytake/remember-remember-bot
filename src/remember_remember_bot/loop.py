import time

import deltachat

from remember_remember_bot.commands import remind_chat, activate_chat, reply, store_file, send_help
from remember_remember_bot.util import check_new_day, update_day


def loop(ac: deltachat.Account):
    current_day = 0
    while True:
        if check_new_day(current_day):
            for chat in ac.get_chats():
                remind_chat(chat)
        current_day = update_day()
        for msg in ac.get_fresh_messages():
            handle_incoming_message(msg)
        time.sleep(1)


def handle_incoming_message(msg: deltachat.Message):
    print(str(msg.chat.id), msg.text)
    if "/start" in msg.text:
        activate_chat(msg)
    elif "/stop" in msg.text:
        msg.chat.set_ephemeral_timer(0)
        reply(msg.chat, "I will stop sending you messages now.", quote=msg)
    elif "/file" in msg.text:
        if msg.filename:
            store_file(msg)
        else:
            activate_chat(msg)
    else:
        send_help(msg)
    msg.mark_seen()
