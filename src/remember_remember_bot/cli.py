import click
import remember_remember_bot.loop
import remember_remember_bot.commands


@click.command()
@click.option("--email", type=str, default=None, help="the email account for the bot")
@click.option(
    "--password", type=str, default=None, help="the password of the email account"
)
@click.option(
    "--db", type=str, default="remember.db/db.sqlite", help="path to the bot's database"
)
@click.option(
    "--debug", is_flag=True, default=False, help="show low level delta chat ffi events"
)
@click.pass_context
def run(ctx, email: str, password: str, db: str, debug: bool):
    """Remember, remember, the 5th of November... a Delta Chat bot to send daily reminders.

    :param ctx: the click context
    :param email: the email account for the bot, e.g. bot@example.org
    :param password: the password for the email account
    :param db: the path to the database
    :param debug: whether to show low-level deltachat FFI events
    """
    ac = remember_remember_bot.commands.setup(email, password, db, debug)
    remember_remember_bot.loop.loop(ac)


def main():
    run(auto_envvar_prefix="REMEMBER")


if __name__ == "__main__":
    main()
